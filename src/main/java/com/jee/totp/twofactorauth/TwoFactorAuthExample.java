/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jee.totp.twofactorauth;

/**
 *
 * @author Jeeva
 */
public class TwoFactorAuthExample {

    /**
     * @param args the command line arguments
     */
    //private static final String TOTP_URI_FORMAT = "otpauth://totp/%s@%s?secret=%s&issuer=Solverminds&algorithm=SHA1&digits=6&period=90";
    private static final String TOTP_URI_FORMAT = "otpauth://totp/%s@%s?secret=%s&digits=6&period=30";
    private static final String IMAGE_PATH = "C:/";

    public static void main(String[] args) throws Exception {

        // String base32Secret = twoFactorAuthUtil.generateBase32Secret();
        TimeBasedOneTimePasswordUtil.generateBase32Secret();
        String base32Secret = "NY4A5CPJZ46LXZCK";

        System.out.println("secret = " + base32Secret);

        // this is the name of the key which can be displayed by the authenticator program
        String userid = "jeeva";
        String host = "developement";
        String keyId = userid + "@" + host;
        // generate the QR code

        // we can display this image to the user to let them load it into their auth program
        // we can use the code here and compare it against user input
        String code = TimeBasedOneTimePasswordUtil.generateCurrentNumberString(base32Secret);
        String totpMessage = String.format(TOTP_URI_FORMAT, userid, host, base32Secret);
        // Image URL
        System.out.println("Image url = " + TimeBasedOneTimePasswordUtil.qrImageUrl(keyId, base32Secret));
        // static image
        QRCode.generateQRCode(totpMessage, IMAGE_PATH + System.currentTimeMillis() + ".png");


        /*
         * this loop shows how the number changes over time
         */
        while (true) {
            long diff = TimeBasedOneTimePasswordUtil.DEFAULT_TIME_STEP_SECONDS
                    - ((System.currentTimeMillis() / 1000) % TimeBasedOneTimePasswordUtil.DEFAULT_TIME_STEP_SECONDS);
            code = TimeBasedOneTimePasswordUtil.generateCurrentNumberString(base32Secret);
            System.out.println("Secret code = " + code + ", change in " + diff + " seconds");
            Thread.sleep(1000);
        }
    }

}
